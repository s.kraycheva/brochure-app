import React, {useContext } from 'react'
import Recipe from './Recipe'
import { RecipeContext } from '../App'
import "../css/app.css"

const RecipeList = ({ recipes }) => {
    const {handleRecipesAdd}=useContext(RecipeContext)
  return (
      <div className='recipe-list'>
      <>
        <div>
            {recipes.map(recipe => {
                return < Recipe key={recipe.id}
                    {...recipe} />
            })}
        </div>
         <div className='recipe-list__add-recipe-btn-container'>
          <button className='btn btn--primary 'onClick={handleRecipesAdd}>Add Recipe</button>
          </div>
      </>
      </div>
  )
}

export default RecipeList