import React from 'react'


const RecipeIngredient = (props) => {
  const { ingredient, handleIngredientChange,handleIngredientDelete } = props
  
  function handleChange(change) {
    handleIngredientChange(ingredient.id,{ ...ingredient, ...change });
    //new recipe-object plus changes
  }
  return (
      <>
          <input type="text" className='recipe-edit__input'onChange={(e)=>handleChange({name:e.target.name})} value={ingredient.name} />
          <input type="text" className='recipe-edit__input' onChange={(e)=>handleChange({amount:e.target.amount})} value={ingredient.amount}/>
          <button className='btn btn--danger'onClick={()=>handleIngredientDelete(ingredient.id)} >&times;</button>
      </>
  )
}

export default RecipeIngredient