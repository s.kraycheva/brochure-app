import React, { useContext} from 'react'
import IngredientList from './IngredientList'
import "../css/app.css"
import { RecipeContext } from '../App'
import { useEffect } from 'react';

function Recipe(props) {
  const { handleRecipesDeletes,handleSelectedRecipes } = useContext(RecipeContext);
  const { id,
    name,
    cookTime,
    servings,
    instructions,
    ingredients,
  } = props

  useEffect(() => {
    return (() => {
      
    })
  })
  return (
    <div className='recipe'>
      <div className='recipe-header'>
        <h3 className='recipe-title'>{name}</h3>
        <div>
          <button className='btn btn--primary mr-1 'onClick={()=>handleSelectedRecipes(id)}>edit</button>
          <button  className='btn btn--danger' onClick={()=>handleRecipesDeletes(id)}>delete</button>
        </div>
      </div>
      <div className='recipe-row'>
        <span className='recipe-label'>Cook Time:</span>
        <span className='recipe-value'>Cook Time:{cookTime}</span>
      </div>
      <div className='recipe-row' >
        <span className='recipe-label'>Servings</span>
        <span className='recipe-value'>{servings}</span>
      </div><div className='recipe-row'>
        <span className='recipe-label' >Instructions:</span>
        <div className='recipe-value recipe__instructions recipe-value--intended'>
          {instructions}
        </div>
      </div>
      <div className='recipe-row'>
        <span className='recipe-label'>Ingredients:</span>
        <div className='recipe-value'>
          <IngredientList ingredients={ingredients}/>
        </div>
      </div>
      
      </div>
  )
}

export default Recipe