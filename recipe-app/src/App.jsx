import { useState } from 'react'
import React from 'react';
import './css/app.css'
import RecipeList from './components/RecipeList'
import { v4 as uuidv4 } from 'uuid';
import { useEffect } from 'react';
import RecipeEdit from './components/RecipeEdit';

export const RecipeContext = React.createContext();
const LOCAL_STORAGE_KEY = "CookWithM.com"

 const sampleRecipes = [{
  id: uuidv4(),
  name: "pork",
  servings: 3,
  cookTime: "1:20",
  instructions: "1.Put salt\n2.Cook\n3.Eat",
  ingredients: [{
    id: uuidv4(),
    name: 'steak',
    amount: "3 kg"
  },
    {
    id: uuidv4(),
    name: 'salt',
    amount: "3 Tbs"
  }
  ]
}, {
  id: uuidv4(),
  name: "fish",
  servings: 2,
  cookTime: "3:14",
  instructions: "1.Put pepper\n2.Cook\n3.Eat",
  ingredients: [{
    id: uuidv4(),
    name: 'salmon',
    amount: "6 kg"
  },
    {
    id: uuidv4(),
    name: 'pepper',
    amount: "2 Tbs"
  }
  ]
}
]
function App() {
  const [selectedRecipesId, setSelectedRecipesId] = useState();
  const [recipes, setRecipes] = useState(sampleRecipes)
  const selectedRecipe = recipes.find(recipe => recipe.id === selectedRecipesId)
  
  useEffect(() => {
      try {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(recipes))
      } catch (error) {
        console.error(error.message)
      }
  }, [recipes])

  useEffect(() => {
    try {
      const recipeJson = localStorage.getItem(LOCAL_STORAGE_KEY)
    if(recipeJson!==null) setRecipes(JSON.parse(JSON.stringify(recipes)))  
  } catch (error) {
    console.error(error.message)
  }
  
  },[])


  
  const recipeContextValue={
    handleRecipesAdd,
    handleRecipesDeletes,
    handleSelectedRecipes,
    handleRecipeChange
   }



  function handleRecipesAdd() {
  const newRecipes = {
    id: uuidv4(),
    name: "",
    servings: 1,
    cookTime: "",
    instructions: "",
    ingredients:[{ id: uuidv4(),
    name: "",
    amount: ""}]
  }
   setSelectedRecipesId(newRecipes.id)
  setRecipes([...recipes,newRecipes])

  
  }

  
  
  function handleRecipesDeletes(id) {
    if (selectedRecipesId !== null && selectedRecipesId == id) {
      setSelectedRecipesId(undefined)
    }
  setRecipes(recipes.filter(recipe=>recipe.id!==id))
  }
  function handleRecipeChange(id, recipe) {
    const newRecipes = [...recipes]
    const index = newRecipes.findIndex(recipe => recipe.id === id)
    newRecipes[index] = recipe;
    setRecipes(newRecipes)
  }
   
  function handleSelectedRecipes(id) {
    setSelectedRecipesId(id)
  }
  return (
    <RecipeContext.Provider value={recipeContextValue}><RecipeList recipes={recipes}
    />
      {selectedRecipe && <RecipeEdit recipe={selectedRecipe} />}
    </RecipeContext.Provider>
    
  )
  
  

}



export default App
